(*****************************************************************************)
(*                                                                           *)
(* Copyright (c) 2022 Miguel Ambrona <mac.ambrona@gmail.com>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Piece = Board.Piece
module Square = Board.Square
module Direction = Board.Direction

type castling_rights = {
  white_long : bool;
  black_long : bool;
  white_short : bool;
  black_short : bool;
}

type t = {
  board : Board.t;
  turn : Color.t;
  castling_rights : castling_rights;
  en_passant : Square.t option;
  halfmove_clock : int option;
  fullmove_number : int;
}

let equal t1 t2 =
  Board.equal t1.board t2.board
  && Color.equal t1.turn t2.turn
  && t1.castling_rights = t2.castling_rights
  && Option.equal Square.equal t1.en_passant t2.en_passant
  && Option.equal Int.equal t1.halfmove_clock t2.halfmove_clock
  && Int.equal t1.fullmove_number t2.fullmove_number

let castling_rights_of_string str =
  {
    white_long = String.contains str 'Q';
    black_long = String.contains str 'q';
    white_short = String.contains str 'K';
    black_short = String.contains str 'k';
  }

let castling_rights_to_string cr =
  let get b symbol = if b then symbol else "" in
  let out =
    String.concat ""
      [
        get cr.white_short "K";
        get cr.white_long "Q";
        get cr.black_short "k";
        get cr.black_long "q";
      ]
  in
  if String.length out = 0 then "-" else out

let en_passant_of_string = function
  | "-" -> None
  | s -> Some (Square.of_string s)

let en_passant_to_string = function None -> "-" | Some s -> Square.to_string s

let remove_brackets s =
  if s.[0] = '(' then String.sub s 1 (String.length s - 2) else s

let halfmove_clock_of_string = function
  | "-" -> None
  | "?" -> Some (-1)
  | s -> Some (int_of_string @@ remove_brackets s)

let halfmove_clock_to_string = function
  | None -> "-"
  | Some i -> if i < 0 then "?" else string_of_int i

let fullmove_number_of_string s = int_of_string @@ remove_brackets s

let fullmove_number_to_string i =
  if i >= 0 then string_of_int i else "(" ^ string_of_int i ^ ")"

let set_piece (p, sq) pos =
  { pos with board = Board.set_piece (p, sq) pos.board }

let remove_piece sq pos = { pos with board = Board.remove_piece sq pos.board }
let piece_at sq pos = Board.piece_at sq pos.board

let king_square c pos =
  let c_king = Piece.make c Piece.king in
  List.find (fun (p, _) -> Piece.equal p c_king) (Board.to_pieces pos.board)
  |> snd

let attackers s c pos =
  (* let's create a bogus knight to be taken *)
  let knight = Piece.make (Color.negate c) Piece.knight in
  let pos = { pos with board = Board.set_piece (knight, s) pos.board } in
  List.filter (fun m -> s = Move.target m) (Move.pseudo_legal_moves c pos.board)
  |> List.map Move.source

let is_attacked s c pos =
  (* let's create a bogus knight to be taken *)
  let knight = Piece.make (Color.negate c) Piece.knight in
  let pos = { pos with board = Board.set_piece (knight, s) pos.board } in
  List.exists (fun m -> s = Move.target m) (Move.pseudo_legal_moves c pos.board)

let checkers pos =
  attackers (king_square pos.turn pos) (Color.negate pos.turn) pos

let is_check pos =
  is_attacked (king_square pos.turn pos) (Color.negate pos.turn) pos

let legal_moves pos =
  let not_into_check m =
    not @@ is_check { pos with board = Move.apply pos.board m }
  in
  let en_passant_ok m =
    if not (Move.is_en_passant pos.board m) then true
    else
      match pos.en_passant with
      | None -> false
      | Some s -> Square.equal s (Move.target m)
  in
  let castling_ok m =
    if not (Move.is_castling pos.board m) then true
    else
      let allowed, must_be_unattacked =
        let open Square in
        if Move.target m = g1 then (pos.castling_rights.white_short, f1)
        else if Move.target m = c1 then (pos.castling_rights.white_long, d1)
        else if Move.target m = g8 then (pos.castling_rights.black_short, f8)
        else if Move.target m = c8 then (pos.castling_rights.black_long, d8)
        else assert false
      in
      allowed
      && (not @@ is_check pos)
      && (not @@ is_attacked must_be_unattacked (Color.negate pos.turn) pos)
  in
  let legal m = not_into_check m && en_passant_ok m && castling_ok m in
  List.filter legal (Move.pseudo_legal_moves pos.turn pos.board)

let moved_pawn board m =
  match Board.piece_at (Move.target m) board with
  | Some p when Piece.piece_type p = Piece.pawn -> true
  | _ -> false

let move pos m =
  let en_passant =
    if not @@ Move.is_double_pawn_push pos.board m then None
    else if Color.is_white pos.turn then Direction.south (Move.target m)
    else Direction.north (Move.target m)
  in
  let halfmove_clock =
    match pos.halfmove_clock with
    | None -> None
    | Some i ->
        let capture = Move.is_capture pos.board m in
        let moved_pawn = Piece.pawn = Move.moved_piece pos.board m in
        if capture || moved_pawn then Some 0 else Some (i + 1)
  in
  let fullmove_number =
    if Color.is_black pos.turn then pos.fullmove_number + 1
    else pos.fullmove_number
  in
  let board = Move.apply pos.board m in
  let turn = Color.negate pos.turn in
  let castling_rights =
    let open Square in
    {
      white_long =
        pos.castling_rights.white_long
        && Board.piece_at e1 pos.board = Some Piece.wK
        && Board.piece_at a1 pos.board = Some Piece.wR;
      white_short =
        pos.castling_rights.white_short
        && Board.piece_at e1 pos.board = Some Piece.wK
        && Board.piece_at h1 pos.board = Some Piece.wR;
      black_long =
        pos.castling_rights.black_long
        && Board.piece_at e8 pos.board = Some Piece.bK
        && Board.piece_at a8 pos.board = Some Piece.bR;
      black_short =
        pos.castling_rights.black_short
        && Board.piece_at e8 pos.board = Some Piece.bK
        && Board.piece_at h8 pos.board = Some Piece.bR;
    }
  in
  { board; turn; castling_rights; en_passant; halfmove_clock; fullmove_number }

let of_fen fen =
  let p1, p2, p3, p4, p5, p6 =
    match String.split_on_char ' ' fen with
    | [ p1; p2; p3; p4; p5; p6 ] -> (p1, p2, p3, p4, p5, p6)
    | _ ->
        raise @@ Invalid_argument "A valid FEN string must contain 6 components"
  in
  {
    board = Board.of_fen p1;
    turn = Color.of_char p2.[0];
    castling_rights = castling_rights_of_string p3;
    en_passant = en_passant_of_string p4;
    halfmove_clock = halfmove_clock_of_string p5;
    fullmove_number = fullmove_number_of_string p6;
  }

let to_fen pos =
  String.concat " "
    [
      Board.to_fen pos.board;
      Char.escaped @@ Color.to_char pos.turn;
      castling_rights_to_string pos.castling_rights;
      en_passant_to_string pos.en_passant;
      halfmove_clock_to_string pos.halfmove_clock;
      fullmove_number_to_string pos.fullmove_number;
    ]

let initial =
  {
    board = Board.initial;
    turn = Color.White;
    castling_rights = castling_rights_of_string "KQkq";
    en_passant = None;
    halfmove_clock = Some 0;
    fullmove_number = 1;
  }

(* This function takes a position and considers all possible scenarios
   about the non-specified castling rights and en-passant options *)
let vary_ep_and_castling ?(vary_ep = true) ?(vary_castling = true)
    ?(both_players = false) pos =
  (* for every pawn in the relative 4-th rank, we may consider it was
     the last move and thus ep is possible in that file *)
  let rel_north s =
    let dir =
      if Color.is_white pos.turn then Direction.north else Direction.south
    in
    Option.get (dir s)
  in
  let rank4_pawns =
    Board.to_pieces pos.board
    |> List.filter (fun (p, s) ->
           Square.in_relative_rank 4 (Color.negate pos.turn) s
           && Piece.piece_type p = Piece.pawn
           && Piece.color p <> pos.turn
           && Board.piece_at (rel_north s) pos.board = None
           && Board.piece_at (rel_north (rel_north s)) pos.board = None)
  in
  let pos_candidates =
    let halfmove_clock_allows_ep =
      match pos.halfmove_clock with None -> true | Some i -> i <= 0
    in
    let checking_squares = checkers pos in
    match pos.en_passant with
    | None when halfmove_clock_allows_ep && vary_ep ->
        pos
        :: List.filter_map
             (fun (_, s) ->
               if List.exists (fun s' -> s' <> s) checking_squares then None
               else Some { pos with en_passant = Some (rel_north s) })
             rank4_pawns
    | _ -> [ pos ]
  in
  (* varying castling rights *)
  let castling_variations c pos =
    let open Square in
    if Color.is_white c then
      let positions = [ pos ] in
      let positions =
        List.concat_map
          (fun p ->
            if p.castling_rights.white_short then [ p ]
            else if
              Board.piece_at e1 pos.board = Some Piece.wK
              && Board.piece_at h1 pos.board = Some Piece.wR
            then
              let cr1 = { p.castling_rights with white_short = true } in
              let cr2 = { p.castling_rights with white_short = false } in
              [
                { p with castling_rights = cr1 };
                { p with castling_rights = cr2 };
              ]
            else [ p ])
          positions
      in
      let positions =
        List.concat_map
          (fun p ->
            if p.castling_rights.white_long then [ p ]
            else if
              Board.piece_at e1 pos.board = Some Piece.wK
              && Board.piece_at a1 pos.board = Some Piece.wR
            then
              let cr1 = { p.castling_rights with white_long = true } in
              let cr2 = { p.castling_rights with white_long = false } in
              [
                { p with castling_rights = cr1 };
                { p with castling_rights = cr2 };
              ]
            else [ p ])
          positions
      in
      positions
    else
      let positions = [ pos ] in
      let positions =
        List.concat_map
          (fun p ->
            if p.castling_rights.black_short then [ p ]
            else if
              Board.piece_at e8 pos.board = Some Piece.bK
              && Board.piece_at h8 pos.board = Some Piece.bR
            then
              let cr1 = { p.castling_rights with black_short = true } in
              let cr2 = { p.castling_rights with black_short = false } in
              [
                { p with castling_rights = cr1 };
                { p with castling_rights = cr2 };
              ]
            else [ p ])
          positions
      in
      let positions =
        List.concat_map
          (fun p ->
            if p.castling_rights.black_long then [ p ]
            else if
              Board.piece_at e8 pos.board = Some Piece.bK
              && Board.piece_at a8 pos.board = Some Piece.bR
            then
              let cr1 = { p.castling_rights with black_long = true } in
              let cr2 = { p.castling_rights with black_long = false } in
              [
                { p with castling_rights = cr1 };
                { p with castling_rights = cr2 };
              ]
            else [ p ])
          positions
      in
      positions
  in
  if not vary_castling then pos_candidates
  else if both_players then
    List.concat_map (castling_variations Color.white) pos_candidates
    |> List.concat_map (castling_variations Color.black)
  else List.concat_map (castling_variations pos.turn) pos_candidates

let rec legal_retractions_aux ?(checks_cnt = 2) pos =
  let not_into_check r =
    not @@ is_check { pos with board = Retraction.apply pos.board r }
  in
  let uncastling_ok r =
    if not (Retraction.is_uncastling pos.board r) then true
    else
      let must_be_unattacked =
        let open Square in
        if Retraction.source r = c1 then [ d1; e1 ]
        else if Retraction.source r = g1 then [ f1; e1 ]
        else if Retraction.source r = c8 then [ d8; e8 ]
        else if Retraction.source r = g8 then [ f8; e8 ]
        else assert false
      in
      List.for_all
        (fun s -> not @@ is_attacked s pos.turn pos)
        must_be_unattacked
  in
  let respected_castling_rights r =
    let cr = pos.castling_rights in
    let s = Retraction.source r in
    let open Square in
    if s = e1 then (not cr.white_long) && not cr.white_short
    else if s = a1 then not cr.white_long
    else if s = h1 then not cr.white_short
    else if s = e8 then (not cr.black_long) && not cr.black_short
    else if s = a8 then not cr.black_long
    else if s = h8 then not cr.black_short
    else true
  in
  let material_ok r =
    let open Piece in
    let count = Board.count in
    let b = Retraction.apply pos.board r in
    let missing_wP = 8 - count wP b in
    let white_promoted =
      count wN b - 2
      + (count wB ~square_color:Color.white b - 1)
      + (count wB ~square_color:Color.black b - 1)
      + (count wR b - 2)
      + (count wQ b - 1)
    in
    let missing_bP = 8 - count bP b in
    let black_promoted =
      count bN b - 2
      + (count bB ~square_color:Color.white b - 1)
      + (count bB ~square_color:Color.black b - 1)
      + (count bR b - 2)
      + (count bQ b - 1)
    in
    missing_wP >= 0
    && white_promoted <= missing_wP
    && missing_bP >= 0
    && black_promoted <= missing_bP
  in
  let retractions = Retraction.pseudo_legal_retractions pos.turn pos.board in
  (* the retraction is a double pawn_unpush iff pos has an en-passant flag *)
  let retractions =
    let filter =
      match pos.en_passant with
      | None -> fun r -> not @@ Retraction.is_double_pawn_unpush pos.board r
      | Some s ->
          let rel_south =
            if Color.is_white pos.turn then Direction.south else Direction.north
          in
          fun r ->
            Retraction.is_double_pawn_unpush pos.board r
            && rel_south s = Some (Retraction.source r)
    in
    List.filter filter retractions
  in
  (* if the halfmove clock is positive in pos, the retraction cannot be
     a capture or a pawn move; on the other hand, it if it is 0, the retraction
     must have been a capture or a pawn move; finally if it is None or negative,
     there is no reatrictions *)
  let retractions =
    match pos.halfmove_clock with
    | None -> retractions
    | Some i ->
        let pawn_move r =
          let p =
            Option.get
              (Board.piece_at (Retraction.target r)
              @@ Retraction.apply pos.board r)
          in
          Piece.piece_type p = Piece.pawn
        in
        let capture r = Option.is_some (Retraction.uncaptured r) in
        if i > 0 then
          List.filter
            (fun r -> (not @@ pawn_move r) && (not @@ capture r))
            retractions
        else if i = 0 then
          List.filter (fun r -> pawn_move r || capture r) retractions
        else retractions
  in
  let retractions =
    List.filter
      (fun r ->
        not_into_check r && uncastling_ok r
        && respected_castling_rights r
        && material_ok r)
      retractions
  in
  (* finally, let's check for impossible double checks *)
  (* we conjecture that being able to go back 2 (checks_cnt) fullmoves
     is an evidence of the fact that we are not in an impossible double
     check situation (an imaginary check) *)
  let not_impossible_double_check r =
    List.exists
      (fun pos' ->
        if checks_cnt > 0 && is_check pos' then
          legal_retractions_aux ~checks_cnt:(checks_cnt - 1) pos' <> []
        else true)
      (retract pos r)
  in
  List.filter (fun r -> not_impossible_double_check r) retractions

and retract pos r =
  let board = Retraction.apply pos.board r in
  let turn = Color.negate pos.turn in
  let en_passant =
    if Retraction.unenpassant r then Some (Retraction.source r) else None
  in
  let halfmove_clock =
    match pos.halfmove_clock with None -> None | Some i -> Some (i - 1)
  in
  let fullmove_number =
    if Color.is_white pos.turn then pos.fullmove_number - 1
    else pos.fullmove_number
  in
  let castling_rights =
    let open Square in
    if Retraction.is_uncastling pos.board r then
      if Retraction.source r = c1 then
        { pos.castling_rights with white_long = true }
      else if Retraction.source r = g1 then
        { pos.castling_rights with white_short = true }
      else if Retraction.source r = c8 then
        { pos.castling_rights with black_long = true }
      else if Retraction.source r = g8 then
        { pos.castling_rights with black_short = true }
      else assert false
    else pos.castling_rights
  in
  let vary_castling = Retraction.is_uncastling pos.board r in
  { board; turn; castling_rights; en_passant; halfmove_clock; fullmove_number }
  |> vary_ep_and_castling ~vary_castling

let legal_retractions pos = legal_retractions_aux pos

let is_dead =
  let cha_path =
    match Sys.getenv_opt "CHA_PATH" with
    | None -> "../lib/D3-Chess/src/cha"
    | Some path -> path
  in
  let ic, oc = Unix.open_process cha_path in
  let _ = input_line ic in
  fun pos ->
    let unwinnable_for color =
      let cmd = to_fen pos ^ color in
      output_string oc (cmd ^ "\n");
      flush oc;
      let answer = input_line ic in
      String.sub answer 0 10 = "unwinnable"
    in
    if unwinnable_for "white" then unwinnable_for "black" else false

let all_retracted_positions pos =
  let retracted =
    List.concat_map
      (fun r -> List.map (fun p -> (p, r)) @@ retract pos r)
      (legal_retractions pos)
  in
  (* if the position is alive, every retraction will be alive *)
  if not @@ is_dead pos then retracted
  else
    (* if the position is dead, we must check that the retracted position
       is not dead, in virtue of Article 5.2.2 of the FIDE Laws,
       this is the essence of the Dead Reckoning theme *)
    List.filter (fun (p, _r) -> not @@ is_dead p) retracted

let is_legal pos = all_retracted_positions pos <> []
