(*****************************************************************************)
(*                                                                           *)
(* Copyright (c) 2022 Miguel Ambrona <mac.ambrona@gmail.com>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Board

type t = {
  source : Square.t;
  target : Square.t;
  unpromotion : bool;
  unenpassant : bool;
  uncaptured : Piece.piece_type option;
}

let equal = ( = )

let make ?(unpromotion = false) ?(unenpassant = false) ?uncaptured source target
    =
  { source; target; unpromotion; unenpassant; uncaptured }

let source r = r.source
let target r = r.target
let unpromotion r = r.unpromotion
let unenpassant r = r.unenpassant
let uncaptured r = r.uncaptured

let to_string board r =
  let p = Option.get @@ Board.piece_at r.source board in
  let capitalize c = Char.escaped c |> String.capitalize_ascii in
  let p_str = Piece.to_char p |> capitalize in
  let captured_str =
    match r.uncaptured with
    | None -> if r.unenpassant then "x" else "-"
    | Some pt -> "x" ^ capitalize @@ Piece.piece_type_to_char pt
  in
  (if r.unpromotion || Piece.piece_type p = Piece.pawn then "" else p_str)
  ^ Square.to_string r.target ^ captured_str ^ Square.to_string r.source
  ^ (if r.unenpassant then "ep" else "")
  ^ if r.unpromotion then "=" ^ p_str else ""

let moved_piece board r =
  Piece.piece_type @@ Option.get (Board.piece_at r.source board)

let is_double_pawn_unpush board r =
  moved_piece board r = Piece.pawn && Square.king_distance r.source r.target = 2

let is_uncastling board r =
  moved_piece board r = Piece.king && Square.king_distance r.source r.target > 1

let apply board r =
  let p = Option.get @@ Board.piece_at r.source board in
  let c = Piece.color p in
  let new_piece = if r.unpromotion then Piece.make c Piece.pawn else p in
  let uncastling = is_uncastling board r in
  let board = Board.remove_piece r.source board in
  let board = Board.set_piece (new_piece, r.target) board in
  let board =
    match r.uncaptured with
    | None -> board
    | Some pt ->
        let enemy = Piece.make (Color.negate c) pt in
        Board.set_piece (enemy, r.source) board
  in
  let board =
    if r.unenpassant then (
      let rel_south = Direction.(if Color.is_white c then south else north) in
      assert (Piece.piece_type p = Piece.pawn);
      let enemy_pawn = Piece.make (Color.negate c) Piece.pawn in
      Board.set_piece (enemy_pawn, Option.get @@ rel_south r.source) board)
    else board
  in
  let board =
    let open Square in
    if uncastling then (
      let rook_source, rook_target =
        if r.source = c1 then (d1, a1)
        else if r.source = g1 then (f1, h1)
        else if r.source = c8 then (d8, a8)
        else if r.source = g8 then (f8, h8)
        else raise @@ Failure "apply: invalid castling move"
      in
      let rook = Option.get @@ Board.piece_at rook_source board in
      assert (Piece.piece_type rook = Piece.rook);
      assert (Piece.color rook = c);
      Board.remove_piece rook_source board |> Board.set_piece (rook, rook_target))
    else board
  in
  board

let piece_retractions board (p, s) =
  let pt = Piece.piece_type p in
  let c = Piece.color p in
  let occupied s = Option.is_some @@ Board.piece_at s board in
  let free = Fun.negate occupied in
  let rec follow_ray = function
    | [] -> []
    | s :: ray -> if occupied s then [] else s :: follow_ray ray
  in
  let contains p s =
    match Board.piece_at s board with
    | Some p' -> Piece.equal p p'
    | None -> false
  in
  let targets =
    match pt with
    | King -> List.filter (Fun.negate occupied) (Move.king_targets s)
    | Queen -> List.concat_map follow_ray (Move.queen_rays s)
    | Rook -> List.concat_map follow_ray (Move.rook_rays s)
    | Bishop -> List.concat_map follow_ray (Move.bishop_rays s)
    | Knight -> List.filter (Fun.negate occupied) (Move.knight_jumps s)
    | Pawn -> []
  in
  let capturable_pieces =
    let r = Square.rank s in
    let nbrq = Piece.[ knight; bishop; rook; queen ] in
    if r = Rank.of_char '1' || r = Rank.of_char '8' then nbrq
    else Piece.pawn :: nbrq
  in
  let retractions = List.map (fun t -> make s t) targets in
  let capture_retractions =
    List.concat_map
      (fun u ->
        List.map (fun pt -> { u with uncaptured = Some pt }) capturable_pieces)
      retractions
  in
  let retract_pawn =
    (pt = Piece.pawn && (not @@ Square.in_relative_rank 2 c s))
    || (pt <> Piece.king && Square.in_relative_rank 8 c s)
  in
  let pawn_retractions =
    if not retract_pawn then []
    else
      let unpush s = Option.get @@ Move.pawn_direction (Color.negate c) s in
      let t1 = unpush s in
      let targets =
        if occupied t1 then []
        else if Square.in_relative_rank 4 c s && (not @@ occupied (unpush t1))
        then [ t1; unpush t1 ]
        else [ t1 ]
      in
      let unpromotion = pt <> Piece.pawn in
      List.map (fun t -> make ~unpromotion s t) targets
  in
  let pawn_capture_retractions =
    if not retract_pawn then []
    else
      let targets =
        List.filter_map (fun c -> c s) (Move.pawn_captures @@ Color.negate c)
        |> List.filter (fun t -> not @@ occupied t)
      in
      let unpromotion = pt <> Piece.pawn in
      List.concat_map
        (fun t ->
          List.map
            (fun pt -> make ~unpromotion ~uncaptured:pt s t)
            capturable_pieces)
        targets
  in
  (* En passant *)
  let en_passant_retractions =
    if pt <> Piece.pawn || not (Square.in_relative_rank 6 c s) then []
    else if
      occupied (Option.get @@ Direction.north s)
      || occupied (Option.get @@ Direction.south s)
    then []
    else
      let targets =
        List.filter_map (fun c -> c s) (Move.pawn_captures @@ Color.negate c)
        |> List.filter (fun t -> not @@ occupied t)
      in
      List.map (fun t -> make ~unenpassant:true s t) targets
  in
  (* Castling *)
  let castling_retractions =
    if pt <> Piece.king then []
    else
      let make_retraction = make in
      let open Square in
      let open Piece in
      if Color.is_white c then
        let ok_long = s = c1 && contains wR d1 && free b1 && free a1 in
        let ok_shrt = s = g1 && contains wR f1 && free h1 in
        if (ok_long || ok_shrt) && free e1 then [ make_retraction s e1 ] else []
      else
        let ok_long = s = c8 && contains bR d8 && free b8 && free a8 in
        let ok_shrt = s = g8 && contains bR f8 && free h8 in
        if (ok_long || ok_shrt) && free e1 then [ make_retraction s e1 ] else []
  in
  retractions @ capture_retractions @ pawn_retractions
  @ pawn_capture_retractions @ en_passant_retractions @ castling_retractions

let pseudo_legal_retractions turn board =
  let pieces =
    Board.to_pieces board |> List.filter (fun (p, _) -> turn <> Piece.color p)
  in
  List.concat_map (piece_retractions board) pieces
