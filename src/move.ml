(*****************************************************************************)
(*                                                                           *)
(* Copyright (c) 2022 Miguel Ambrona <mac.ambrona@gmail.com>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Board

type t = {
  source : Square.t;
  target : Square.t;
  promotion : Piece.piece_type option;
}

let equal = ( = )
let make ?promotion source target = { source; target; promotion }
let source m = m.source
let target m = m.target
let promotion m = m.promotion

let of_string str =
  let source = Square.of_string (String.sub str 0 2) in
  let target = Square.of_string (String.sub str 2 2) in
  let promotion =
    if String.length str < 5 then None
    else Some (Piece.piece_type_of_char str.[4])
  in
  { source; target; promotion }

let to_string m =
  let promotion_str =
    match m.promotion with
    | None -> ""
    | Some pt -> Piece.piece_type_to_char pt |> Char.escaped
  in
  Square.to_string m.source ^ Square.to_string m.target ^ promotion_str

let is_capture board m = Option.is_some @@ Board.piece_at m.target board

let moved_piece board m =
  Piece.piece_type @@ Option.get (Board.piece_at m.source board)

let is_double_pawn_push board m =
  moved_piece board m = Piece.pawn && Square.king_distance m.source m.target = 2

let is_en_passant board m =
  moved_piece board m = Piece.pawn
  && Square.file m.source <> Square.file m.target
  && not (is_capture board m)

let is_castling board m =
  moved_piece board m = Piece.king && Square.king_distance m.source m.target > 1

let apply board m =
  let p = Option.get @@ Board.piece_at m.source board in
  let c = Piece.color p in
  let new_piece =
    match m.promotion with
    | None -> p
    | Some pt ->
        assert (Piece.piece_type p = Piece.pawn);
        assert (Square.in_relative_rank 8 c m.target);
        Piece.make c pt
  in
  let en_passant = is_en_passant board m in
  let castling =
    Piece.piece_type p = Piece.king
    && Square.king_distance m.source m.target > 1
  in
  let board = Board.remove_piece m.source board in
  let board = Board.set_piece (new_piece, m.target) board in
  let board =
    if en_passant then
      let rel_south = Direction.(if Color.is_white c then south else north) in
      Board.remove_piece (Option.get @@ rel_south m.target) board
    else board
  in
  let board =
    let open Square in
    if castling then (
      let rook_source, rook_target =
        if m.target = c1 && Color.is_white c then (a1, d1)
        else if m.target = g1 && Color.is_white c then (h1, f1)
        else if m.target = c8 && Color.is_black c then (a8, d8)
        else if m.target = g8 && Color.is_black c then (h8, f8)
        else raise @@ Failure "apply: invalid castling move"
      in
      let rook = Option.get @@ Board.piece_at rook_source board in
      assert (Piece.piece_type rook = Piece.rook);
      assert (Piece.color rook = c);
      Board.remove_piece rook_source board |> Board.set_piece (rook, rook_target))
    else board
  in
  board

module SquareMap = Map.Make (Board.Square)

let rec build_ray dir s =
  match dir s with None -> [] | Some t -> t :: build_ray dir t

let build_rays directions =
  List.fold_left
    (fun acc s ->
      let rays = List.map (fun dir -> build_ray dir s) directions in
      let rays = List.filter (fun ray -> ray <> []) rays in
      SquareMap.add s rays acc)
    SquareMap.empty Board.squares

let rook_directions = Direction.[ north; south; east; west ]

let bishop_directions =
  Direction.[ north_east; north_west; south_east; south_west ]

let queen_directions = rook_directions @ bishop_directions

let knight_directions s =
  let open Direction in
  [
    Option.bind (north_east s) north;
    Option.bind (north_east s) east;
    Option.bind (north_west s) north;
    Option.bind (north_west s) west;
    Option.bind (south_east s) south;
    Option.bind (south_east s) east;
    Option.bind (south_west s) south;
    Option.bind (south_west s) west;
  ]
  |> List.filter_map Fun.id

let king_directions s =
  List.map (fun dir -> dir s) queen_directions |> List.filter_map Fun.id

let pawn_direction c =
  if Color.is_white c then Direction.north else Direction.south

let pawn_captures c =
  let open Direction in
  if Color.is_white c then [ north_east; north_west ]
  else [ south_east; south_west ]

let queen_rays_map = build_rays queen_directions
let rook_rays_map = build_rays rook_directions
let bishop_rays_map = build_rays bishop_directions

let knight_jumps_map =
  List.fold_left
    (fun acc s -> SquareMap.add s (knight_directions s) acc)
    SquareMap.empty Board.squares

let king_targets_map =
  List.fold_left
    (fun acc s -> SquareMap.add s (king_directions s) acc)
    SquareMap.empty Board.squares

let queen_rays s = SquareMap.find s queen_rays_map
let rook_rays s = SquareMap.find s rook_rays_map
let bishop_rays s = SquareMap.find s bishop_rays_map
let king_targets s = SquareMap.find s king_targets_map
let knight_jumps s = SquareMap.find s knight_jumps_map

let contains p s t =
  match Board.piece_at s t with Some p' -> Piece.equal p p' | None -> false

let square_is_empty s t = Option.is_none @@ Board.piece_at s t

let piece_moves board (p, s) =
  let pt = Piece.piece_type p in
  let c = Piece.color p in
  let is_ally p = Color.equal c (Piece.color p) in
  let occupied s = Option.is_some @@ Board.piece_at s board in
  let contains_ally s =
    match Board.piece_at s board with None -> false | Some p -> is_ally p
  in
  let contains_enemy s =
    match Board.piece_at s board with
    | None -> false
    | Some p -> not (is_ally p)
  in
  let rec follow_ray = function
    | [] -> []
    | s :: ray ->
        if contains_ally s then []
        else if contains_enemy s then [ s ]
        else s :: follow_ray ray
  in
  let en_passant_capture t =
    let rel_south = Direction.(if Color.is_white c then south else north) in
    if (not @@ Square.in_relative_rank 6 c t) || occupied t then false
    else
      match Board.piece_at (Option.get @@ rel_south t) board with
      | None -> false
      | Some p -> (not @@ is_ally p) && Piece.piece_type p = Piece.pawn
  in
  let targets =
    match pt with
    | King -> List.filter (Fun.negate contains_ally) (king_targets s)
    | Queen -> List.concat_map follow_ray (queen_rays s)
    | Rook -> List.concat_map follow_ray (rook_rays s)
    | Bishop -> List.concat_map follow_ray (bishop_rays s)
    | Knight -> List.filter (Fun.negate contains_ally) (knight_jumps s)
    | Pawn ->
        let pushes =
          let push s = Option.get @@ pawn_direction c s in
          let t1 = push s in
          if occupied t1 then []
          else if Square.in_relative_rank 2 c s && (not @@ occupied (push t1))
          then [ t1; push t1 ]
          else [ t1 ]
        in
        let captures =
          List.filter_map (fun capt -> capt s) (pawn_captures c)
          |> List.filter (fun t -> contains_enemy t || en_passant_capture t)
        in
        pushes @ captures
  in
  let moves =
    List.map (fun t -> { source = s; target = t; promotion = None }) targets
  in
  (* Promotions *)
  let promotion = pt = Piece.pawn && Square.in_relative_rank 7 c s in
  let moves =
    if promotion then
      let pts = Piece.[ queen; rook; bishop; knight ] in
      List.concat_map
        (fun m -> List.map (fun pt -> { m with promotion = Some pt }) pts)
        moves
    else moves
  in
  (* Castling *)
  let castling_move k1 k2 r1 must_be_empty =
    if
      s = k1
      && contains Piece.(make c king) k1 board
      && contains Piece.(make c rook) r1 board
      && List.for_all (fun sq -> square_is_empty sq board) must_be_empty
    then Some { source = k1; target = k2; promotion = None }
    else None
  in
  let open Square in
  let king_castle =
    if Color.is_white c then castling_move e1 g1 h1 [ f1; g1 ]
    else castling_move e8 g8 h8 [ f8; g8 ]
  in
  let queen_castle =
    if Color.is_white c then castling_move e1 c1 a1 [ b1; c1; d1 ]
    else castling_move e8 c8 a8 [ b8; c8; d8 ]
  in
  Option.(to_list king_castle @ to_list queen_castle) @ moves

let pseudo_legal_moves turn board =
  let pieces =
    Board.to_pieces board
    |> List.filter (fun (p, _) -> Color.equal turn (Piece.color p))
  in
  List.concat_map (piece_moves board) pieces
