# ChessML

A chess library written in OCaml, oriented to creating and solving chess
compositions with especial emphasis on retrograde analysis.

## Installation

Install and init [opam](https://opam.ocaml.org/), if you haven't already.

Then, after cloning the ChessML repository:

1. Install [CHA](https://github.com/miguel-ambrona/D3-Chess) by running
`make cha` from the `lib/` directory.

2. Compile ChessML by running `dune build` (from the root folder).

3. Test the tool with `dune test` (from the root folder).