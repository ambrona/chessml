(*****************************************************************************)
(*                                                                           *)
(* Copyright (c) 2022 Miguel Ambrona <mac.ambrona@gmail.com>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Chessml

module Test_Move = struct
  module Square = Board.Square
  module Piece = Board.Piece
  open Retraction

  let test_pseudo_legal_retractions () =
    let board = Board.of_fen "8/5p2/5P2/8/1p6/kP2pp2/1pKpP3/3B3b" in
    List.iter
      (fun r ->
        Format.printf "retraction: %s\n" @@ Retraction.to_string board r)
      (pseudo_legal_retractions Color.white board)

  let test_all_retracted_positions () =
    let pos = Position.of_fen "7K/4Pk2/3p1nq1/8/8/8/8/8 w - - -1 1" in
    List.iter
      (fun (p, _r) -> Format.printf "%s\n" @@ Position.to_fen p)
      (Position.all_retracted_positions pos)

  let tests =
    Alcotest.
      ( "Retraction",
        [
          test_case "pseudo_legal_retractions" `Quick
            test_pseudo_legal_retractions;
          test_case "pseudo_legal_retractions" `Quick
            test_all_retracted_positions;
        ] )
end

let () =
  let open Alcotest in
  run "Retraction" [ Test_Move.tests ]
