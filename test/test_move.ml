(*****************************************************************************)
(*                                                                           *)
(* Copyright (c) 2022 Miguel Ambrona <mac.ambrona@gmail.com>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Chessml

module Test_Move = struct
  module Square = Board.Square
  module Piece = Board.Piece
  open Move

  let e2e4 = make Square.e2 Square.e4
  let e7e5 = make Square.e7 Square.e5

  let board_after_e2e4 =
    Board.of_fen "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR"

  let h7h8q = make ~promotion:Piece.queen Square.h7 Square.h8

  let test_equal () =
    assert (equal e2e4 e2e4);
    assert (not @@ equal e2e4 e7e5)

  let test_apply () =
    let board = apply Board.initial e2e4 in
    assert (Board.(equal board board_after_e2e4))

  let test_pseudo_legal_moves () =
    List.iter
      (fun m -> Format.printf "move: %s\n" @@ Move.to_string m)
      (pseudo_legal_moves Color.white board_after_e2e4);
    assert (20 = List.length @@ pseudo_legal_moves Color.white Board.initial);
    assert (20 = List.length @@ pseudo_legal_moves Color.black board_after_e2e4);
    assert (30 = List.length @@ pseudo_legal_moves Color.white board_after_e2e4)

  let test_of_string () =
    assert (equal e2e4 @@ of_string "e2e4");
    assert (equal e7e5 @@ of_string "e7e5");
    assert (equal h7h8q @@ of_string "h7h8q")

  let test_to_string () =
    assert (String.equal "e2e4" @@ to_string e2e4);
    assert (String.equal "e7e5" @@ to_string e7e5);
    assert (String.equal "h7h8q" @@ to_string h7h8q)

  let tests =
    Alcotest.
      ( "Move",
        [
          test_case "equal" `Quick test_equal;
          test_case "apply" `Quick test_apply;
          test_case "pseudo_legal_moves" `Quick test_pseudo_legal_moves;
          test_case "of_string" `Quick test_of_string;
          test_case "to_string" `Quick test_to_string;
        ] )
end

let () =
  let open Alcotest in
  run "Move" [ Test_Move.tests ]
