open Chessml

module Test_Position = struct
  open Position

  let position_fens =
    [
      "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1";
      "r1b1kb1r/6pp/p1pppn2/4P1B1/8/q1N5/P1PQ2PP/1R2KB1R b Kkq - 0 13";
    ]

  let test_position_of_fen () =
    let initial_fen =
      "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
    in
    assert (Position.(equal initial @@ of_fen initial_fen))

  let test_position_to_fen () =
    List.iter
      (fun fen -> assert (fen = Position.(to_fen @@ of_fen fen)))
      position_fens

  let rec perft depth pos =
    if depth <= 0 then 1
    else if depth = 1 then List.length (Position.legal_moves pos)
    else
      let childs = List.map (Position.move pos) (Position.legal_moves pos) in
      List.fold_left Int.add 0 @@ List.map (perft (depth - 1)) childs

  let test_perft ?(quick = false) () =
    let fens =
      [
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
        "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1";
        "8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 1";
        "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1";
        "rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8";
        "r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 \
         10";
      ]
    in
    let expected =
      [
        [ 20; 400; 8_902; 197_281; 4_865_609 ];
        [ 48; 2_039; 97_862; 4_085_603 ];
        [ 14; 191; 2_812; 43_238; 674_624 ];
        [ 6; 264; 9467; 422_333 ];
        [ 44; 1_486; 62_379; 2_103_487 ];
        [ 46; 2_079; 89_890; 3_894_594 ];
      ]
    in
    List.iter2
      (fun fen ->
        List.iteri (fun i n ->
            if quick && n > 100_000 then ()
            else assert (perft (i + 1) (of_fen fen) = n)))
      fens expected

  let test_retract () =
    let tests =
      [
        ("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1", 0);
        ("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - - 1", 4);
        ("8/5p2/5P2/8/1p6/kP2pp2/1pKpP3/3B4 w - - - 50", 54);
        ("8/5p2/5P2/8/1p6/kP2pp2/1pKpP3/3B4 w - - 1 50", 2);
        ("8/5p2/5P2/8/1p6/kP2pp2/1pKpP3/3B4 w - - 0 50", 52);
        ("8/8/8/8/8/6k1/5n2/3n1RK1 b - - - 50", 9);
        ("8/8/8/8/8/6k1/5n2/3nRrK1 w - - - 50", 8);
        ("8/8/3P4/8/8/8/7P/5k1K b - - - 50", 13);
        ("k1K5/p7/8/8/8/7p/8/8 w - - - 50", 7);
        ("k1K5/p7/8/7p/8/8/8/8 w - - - 50", 6);
        ("k1K5/p7/8/7p/8/8/8/8 w - h6 - 50", 1);
        ("8/8/8/p7/P5p1/4p1Pk/P3P2P/5bBK w - - - 50", 33);
        ("8/8/8/p7/P5p1/4p1Pk/P3P2P/5bBK w - a6 - 50", 1);
        ("8/8/8/8/6P1/5N1p/5K1P/4N1Bk w - - - 50", 2);
        ("8/8/8/8/6P1/5N1p/5K1P/4N1Bk w - - 0 50", 2);
        ("8/8/8/8/6P1/5N1p/5K1P/4N1Bk w - - 1 50", 0);
        ("4k3/8/8/8/5bn1/8/P4PP1/Rn1n1RK1 b - - - 50", 7);
        ("4k3/2p5/8/p1p5/5bn1/8/P4PP1/Rn1n1RK1 b - - - 50", 12);
        ("4k3/8/8/p1p5/5bn1/8/P4PP1/Rn1n1RK1 b - - - 50", 17);
        ("4k3/8/8/8/8/8/P4PPn/Rn1n1RK1 b - - - 50", 3);
        ("1R6/1bNB2n1/pk2P3/pN2p2p/3pP2K/3P3p/5B1P/6n1 b - - 2 50", 20);
        ("1R6/1bNB2n1/pk2P3/pN2p2p/3pP2K/3P3p/5B1P/6n1 b - - 1 50", 42);
        ("1R6/1bNB2n1/pk2P3/pN2p2p/3pP2K/3P3p/5B1P/6n1 b - - 0 50", 242);
        ("1R6/1bNB2n1/pk2P3/pN2p2p/3pP2K/3P3p/5B1P/6n1 b - - - 50", 284);
        ("k7/1n6/8/4p3/3K4/8/8/8 w - - 1 50", 0);
        ("k7/1n6/8/4p3/3K4/8/8/8 w - - 0 50", 11);
        ("k7/1n6/8/4p3/3K4/8/8/8 w - - - 50", 11);
        ("k7/1n6/8/4p3/3K4/8/8/8 w - e6 - 50", 1);
      ]
    in
    let predecessors pos =
      let retractions = Position.legal_retractions pos in
      List.concat_map (Position.retract pos) retractions
    in
    assert (
      List.for_all
        (fun (fen, n) -> n = List.length @@ predecessors (of_fen fen))
        tests)

  let test_is_dead () =
    let dead =
      [
        "8/1k5B/7b/8/1p1p1p1p/1PpP1P1P/2P3K1/N3b3 b - - - 1";
        "4K3/8/8/8/8/p1p2p1p/P1pppp1P/bnrqkrnb b - - - 1";
        "Bb1k1b2/bKp1p1p1/1pP1P1P1/1P6/p5P1/P7/8/8 b - - - 1";
        "7k/8/1p6/1Pp5/2Pp4/pB1Pp1p1/P1B1P1P1/1B1B2K1 b - - - 1";
        "8/8/7p/5p1P/5p1K/5Pp1/6P1/5kb1 b - - - 1";
        "1k6/1P5p/BP3p2/1P6/8/8/5PKP/8 b - - - 1";
        "k7/Q6r/2b5/1pBp1p1p/1P1P1P1P/KP6/1P6/8 b - - - 1";
        "1k6/p1p1p1p1/P1P1P1P1/p1p1p1p1/8/8/P1P1P1P1/4K3 w - - - 1";
      ]
    in
    let alive =
      [
        "5brk/4p1p1/3pP1P1/1B1P2p1/3p2p1/3P4/4K1P1/8 w - - - 1";
        "8/1p4p1/1Pp3p1/k1P3p1/1pP3Pb/1P4p1/6P1/7K w - - - 1";
        "7b/1k5B/7b/8/1p1p1p1p/1PpP1P1P/2P3K1/N7 b - - - 1";
        "k1bK4/1p1p4/1PpPp3/2P1Pp2/2p1pP2/2p1P3/2P5/8 w - - - 1";
        "Bb1k1b2/bKp1p1p1/1pP1P1P1/pP6/6P1/P7/8/8 b - - - 1";
        "7k/8/1p6/1Pp5/2Pp4/pB1Pp1p1/P1B1P1P1/3B2K1 b - - - 1";
        "2b5/1p6/pPp3k1/2Pp3p/P2PpBpP/4P1P1/5K2/8 b - - - 1";
        "rnb1b3/pk1p4/p1pPp1p1/P1P1P1P1/RBP5/P7/5B2/7K w - - - 1";
      ]
    in
    assert (List.for_all is_dead (List.map of_fen dead));
    assert (List.for_all (Fun.negate is_dead) (List.map of_fen alive))

  let test_all_retracted_positions () =
    let tests =
      [
        ("5bk1/4p1p1/4P1P1/7K/8/8/8/8 w - - - 1", 1);
        ("8/8/8/8/6P1/5N1p/5K1P/4N1Bk w - - - 1", 1);
      ]
    in
    assert (
      List.for_all
        (fun (fen, n) ->
          n = List.length @@ all_retracted_positions (of_fen fen))
        tests)

  let tests =
    Alcotest.
      ( "Position",
        [
          test_case "of_fen" `Quick test_position_of_fen;
          test_case "to_fen" `Quick test_position_to_fen;
          test_case "perft" `Quick @@ test_perft ~quick:true;
          test_case "retract" `Quick test_retract;
          test_case "dead" `Quick test_is_dead;
          test_case "all_retracted_positions" `Quick
            test_all_retracted_positions;
        ] )
end

let () =
  let open Alcotest in
  run "Position" [ Test_Position.tests ]

(* let board_fens =
 *   [
 *     "1R6/1bNB2n1/pk2P3/pN2p2p/3pP2K/3P3p/5B1P/6n1";
 *     "8/5PQ1/6P1/4B1P1/3P1PP1/4kP1N/B3P3/RN2K2R";
 *     "1K6/2R1p3/r1P5/1N6/1Nk5/P1nnP3/3P4/2b5";
 *     "8/7Q/8/4BB2/2PP1P2/3NkN2/PP2P1P1/4K2R";
 *     "8/3R2K1/4kP1P/3R3P/3P2BP/4N1BP/7P/8";
 *     "8/8/2Q1N3/3B4/2PP4/3Nk2P/2P5/B2RK2R";
 *     "4b1n1/3pPk2/3PpN1P/5PBK/6p1/8/8/8";
 *     "8/5p2/5P2/8/1p6/kP2pp2/1pKpP3/3B4";
 *     "8/8/8/p7/P5p1/4p1Pk/P3P2P/5bBK";
 *     "6br/4Bp1k/5P2/5PpK/4B1P1/8/8/8";
 *     "5bk1/4p1p1/4P1P1/7K/8/8/8/8";
 *     "8/8/8/8/7R/7R/3RR1B1/R3K1kB";
 *     "8/8/8/8/6P1/5N1p/5K1P/4N1Bk";
 *     "8/8/8/pN1k4/3p1PB1/1K6/8/8";
 *     "3k4/8/8/1N3P2/3B4/8/3K4/8";
*   ] *)
