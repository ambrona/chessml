(*****************************************************************************)
(*                                                                           *)
(* Copyright (c) 2022 Miguel Ambrona <mac.ambrona@gmail.com>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Chessml

let read_fen : string -> Position.t list =
 fun fen ->
  let p1, p2, p3, p4, p5, p6 =
    match String.split_on_char ' ' fen with
    | [ p1; p2; p3; p4; p5; p6 ] -> (p1, p2, p3, p4, p5, p6)
    | [ p1; p2; p3; p4 ] -> (p1, p2, p3, p4, "?", "1")
    | _ ->
        raise
        @@ Invalid_argument "A valid FEN string must contain 4 or 6 components"
  in
  let turns = if p2 = "?" then [ "w"; "b" ] else [ p2 ] in
  let castling, vary_castling = if p3 = "?" then ("-", true) else (p3, false) in
  let ep, vary_ep = if p4 = "?" then ("-", true) else (p4, false) in
  let positions =
    List.map
      (fun t ->
        let fen = String.concat " " [ p1; t; castling; ep; p5; p6 ] in
        Position.of_fen fen)
      turns
  in
  List.concat_map
    (Position.vary_ep_and_castling ~vary_ep ~vary_castling
       ~both_players:vary_castling)
    positions
  |> List.filter Position.is_legal

let rec process_command cmd =
  let words = String.split_on_char ' ' cmd in
  let instruction = List.hd words in
  let fen = String.concat " " @@ List.tl words in
  if instruction = "quit" then ()
  else
    let n =
      match instruction with
      | "complete" ->
          let positions = read_fen fen in
          List.iter
            (fun p -> Format.printf "%s\n" @@ Position.to_fen p)
            positions;
          List.length positions
      | "retract" ->
          let pos = Position.of_fen fen in
          let retractions = Position.all_retracted_positions pos in
          List.iter
            (fun (p, r) ->
              Format.printf "%s retraction %s\n" (Position.to_fen p)
                (Retraction.to_string pos.board r))
            retractions;
          List.length retractions
      | _ -> raise @@ Invalid_argument "Unknown command"
    in
    Format.printf "nsols %d\n" n;
    Format.print_flush ();
    wait_for_command ()

and wait_for_command () = process_command @@ input_line stdin

let main = wait_for_command ()
